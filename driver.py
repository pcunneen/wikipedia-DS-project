from path_analysis import pathExistence
from shortest_path import djikstra
import random
import json
import os

node_count = 0
density = 0
randomness = 0
min_weight = 1
max_weight = 100

command_dict = [
    ('help', 'show help menu'),
    ('exit', 'exit program'),
    ('new graph', 'generate a new graph'),
    ('view graph', 'print current graph'),
    ('path existence', 'find whether a path exists between two nodes'),
    ('is connected', 'discover whether the graph is connected'),
    ('most central', 'calculate the most central node of the graph'),
    ('shortest path', 'Calculate the shortest path between two nodes')
    ]

def begin(node_count):
    print 'Enter a number between 1 and 10 to specify the graph density'
    density = int(raw_input('--> '))
    if density < 1 or density > 10:
        print 'learn to follow instructions and try again'
        exit()
    graph = buildGraph(node_count, density)
    return graph

def buildGraph(node_count, density):
    graph = {}
    for i in xrange(node_count):
        graph[i] = list()
    num_connections = float(density) * .1 * node_count
    for i in xrange(node_count):
        rand_offset = random.randint(-1 * randomness, randomness)
        num_connections += rand_offset
        current_connections = list()
        for j in xrange(int(num_connections)):
            start_node = j
            end_node = random.randint(0, node_count - 1)
            weight = random.randint(min_weight, max_weight)
            while end_node in current_connections:
                end_node = random.randint(0, node_count - 1)
                weight = random.randint(min_weight, max_weight)
            graph[i].append((end_node, weight))
            current_connections.append(end_node)
    return graph

if __name__ == '__main__':
    command = "Start"
    print 'Enter the number of nodes in your graph: '
    node_count = int(raw_input('--> '))
    graph = begin(node_count)
    with open('graph.json', 'w') as fp:
        json.dump(graph, fp)
    graph = {int(key): graph[key] for key in graph}
    while command != 'exit':
        node = None
        print "Commands:"
        for pair in command_dict:
            print "\t\"%s\" -- %s" %(pair[0], pair[1])
        print "Enter Command: "
        command = raw_input('--> ').lower()
        if command == 'new graph':
            print 'Enter the number of nodes in your graph: '
            node_count = int(raw_input('--> '))
            graph = {}
            graph = begin(node_count)
            with open('graph.json', 'w') as fp:
                json.dump(graph, fp)

        elif command == 'help':
            print '------------------------'
            for k, v in command_dict:
                print '{}: {}'.format(k, v)
            print '------------------------'

        elif command == 'path existence':
            print 'Enter starting node'
            start_node = int(raw_input('--> '))
            print 'Enter ending node'
            end_node = int(raw_input('--> '))
            if pathExistence(graph, start_node, end_node):
                print "A path does exist from %d to %d" %(start_node, end_node)
            else:
                print "A path does not exist from %d to %d" %(start_node, end_node)

        elif command == 'shortest path':
            print 'Enter starting node'
            start_node = int(raw_input('--> '))
            print 'Enter ending node'
            end_node = int(raw_input('--> '))
            shortest_path_output = djikstra(graph, start_node, end_node)
            print "path = ", '->'.join([str(node) for node in shortest_path_output[0]])
            print "Total weight = ", shortest_path_output[1]

        elif command == 'view graph':
            for key in graph:
                print "{}: {}".format(key, graph[key])

        elif command == 'is connected':
            os.system("python is_connected.py")
        elif command == 'most central':
            os.system("python central_node.py %s" %(node_count))

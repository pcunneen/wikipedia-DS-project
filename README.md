Our project is focused on graph analysis. By running driver.py, you can generate random graphs of any size and varying levels of connectivity. You can view the graph, generate a new one, and run several different algorithms. You can check for the existence of a path between two nodes, find the shortest path (if it exists) between two nodes, and discover whether or not the graph is connected.

The driver program for our project can be run without any external dependencies. In the program, follow the prompts to proceed and enter help if you are having any issues

The second part of our project is an algorithm to discover the most central node of the graph. It can be run either through driver or on its own. This can be a computationaly expensive process so we parallelize the task by using work_queue. This lets us run algorithms that might normally take several hours in just a few minutes.

To run the central_node program with work_queue,

$ python central_node.py number_of_nodes

Then, submit workers to work_queue as follows:

$ condor submit workers master.somewhere.edu PORT NUMBER_OF_NODES

Below is our benchmark data. It compares the different search and algorithm run times with different sizes and sparsenesses. The g100_10 graph is a graph with 100 nodes and a density setting of 10, meaning that each node has a connection between every other node while g_100_1 would ony have a tenth as many connections.

|Graph type|BFS average time|DFS average time|Shortest path average time|Path existence average time|
|---------:|:--------------:|:--------------:|:-------------------------|---------------------------|
| g100_10 | 0.00945354700089 | 0.00125017166138 | 0.0174568843842 | 0.000617945194244
| g100_5 | 0.00484785318375 | 0.000839786529541 | 0.0063806438446 | 0.000271029472351
| g100_1 | 0.00186891317368 | 0.000198938846588 | 0.00181853532791 | 0.000106074810028
| g70_10 | 0.00370680809021 | 0.000587465763092 | 0.00440337657928 | 0.000340895652771
| g70_5 | 0.00302874326706 | 0.000256309509277 | 0.00418945789337 | 0.000168516635895
| g70_1 | 0.00128635406494 | 8.97812843323e-05 | 0.00128679513931 | 8.48960876465e-05
| g40_10 | 0.00144111871719 | 0.000216360092163 | 0.00211253166199 | 0.000132520198822
| g40_5 | 0.00100515127182 | 0.000107717514038 | 0.00148709535599 | 5.90562820435e-05
| g40_1 | 0.000364854335785 | 4.4412612915e-05 | 0.000485587120056 | 3.04222106934e-05
| g10_10 | 0.000125238895416 | 2.52628326416e-05 | 0.000277185440063 | 1.85561180115e-05
| g10_5 | 0.000115990638733 | 2.33697891235e-05 | 0.000184242725372 | 1.37829780579e-05
| g10_1 | 7.01260566711e-05 | 9.26733016968e-06 | 5.21063804626e-05 | 1.03974342346e-05
import Queue


def djikstra(graph, start_node, end_node):
    frontier = Queue.PriorityQueue()
    marked = {}
    frontier.put((0, start_node, start_node))
    while not frontier.empty():
        v = frontier.get()
        if v[1] in marked:
            continue
        marked[v[1]] = (v[2], v[0])
        if v[1] == end_node:
            break
        if v[1] in graph:
            for node in graph[v[1]]:
                frontier.put((node[1] + v[0], node[0], v[1]))

    path = []
    if (end_node not in marked):
        return [[], 0]
    while end_node != start_node:
        path.append(end_node)
        end_node = marked[end_node][0]
    path.append(start_node)
    path.reverse()
    total_distance = 0
    for node in xrange(len(path) - 1):
        cur_node = graph[path[node]]
        for tup in graph[path[node]]:
            if tup[0] == path[node + 1]:
                total_distance += tup[1]
    return [path, total_distance]
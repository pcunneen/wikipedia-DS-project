import dfs

def pathExistence(graph, start_node, end_node):
    visited = {}
    for key in graph:
        visited[key] = False
    queue = list()
    path = list()
    queue.append(start_node)
    while queue:
        current_node  = queue.pop(0)
        if current_node == end_node:
            path.append(current_node)
            return True 
        path.append(current_node)
        for node in graph[current_node]:
            if visited[node[0]] == False:
                queue.append(node[0])
                visited[node[0]] = True
    return False

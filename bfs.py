from Queue import Queue

def bfs(start, end, graph):
    queue = Queue()
    queue.put(start)
    visited = {}
    path = {}
    while not queue.empty():
        parent = queue.get()
        if parent == end:
            return recreate_path(path, start, end)
        elif parent not in graph:
            continue
        visited[parent] = 1
        neighbors = graph.get(parent)
        if not neighbors:
            continue
        neighbors = [node[0] for node in neighbors]
        for node in neighbors:
            if node not in visited and parent != node:
                path[node] = parent
                queue.put(node)
    return False

def recreate_path(path_map, start, end):
    path = [end]
    while end != start:
        end = path_map[end]
        path.append(end)
    return path[::-1]
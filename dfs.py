
def dfs(start, end, graph):
    stack = []
    stack.append(start)
    visited = {}
    path = {}
    while len(stack) > 0:
        parent = stack.pop()
        if parent == end:
            return recreate_path(path, start, end)
        elif parent not in graph:
            continue
        visited[parent] = 1
        neighbors = graph.get(parent)
        neighbors = [node[0] for node in neighbors]
        if not neighbors:
            continue
        for node in neighbors:
            if node not in visited and parent != node:
                path[node] = parent
                stack.append(node)
    return False
def recreate_path(path_map, start, end):
    path = [end]
    while end != start:
        end = path_map[end]
        path.append(end)
    return path[::-1]

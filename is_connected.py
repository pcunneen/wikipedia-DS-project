import json


def Tarjan(graph, num_nodes):
  for node in xrange(num_nodes):
    if not order.get(node):
      strongConnect(node)

def strongConnect(node):
  global order_num
  order_num += 1
  order[node] = order_num
  link[node] = order[node]
  stack.append(node)
  if node in graph.keys():
    for neighbor in graph[node]:
      neighbor = neighbor[0]
      if neighbor not in order:
        strongConnect(neighbor)
        link[node] = min(link[node], link[neighbor])
      elif neighbor in stack:
        link[node] = min(link[node], order[neighbor])
  if link[node] == order[node]:
    n = stack.pop()
    connected_components.append([])
    while n != node:
      connected_components[-1].append(n)
      n = stack.pop()
    connected_components[-1].append(n)


order = {}
link = {}
stack = []
order_num = 0
connected_components = []

if __name__ == "__main__":
  with open("graph.json", "r") as f:
    graph = json.load(f)
  graph = {int(key): graph[key] for key in graph}
  Tarjan(graph, len(graph))
  connected_components = [[str(i) for i in component[::-1]] for component in connected_components]
  if len(connected_components) == 1:
    print "Graph is connected"
    print "path: ", '->'.join(connected_components[0])
  else:
    print "The graph is not connected"
    print "Below are paths through the connected components"
    for i in xrange(len(connected_components)):
      print "Component", i
      print '->'.join(connected_components[i])

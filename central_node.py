import os
import work_queue
import json
import sys
import time

PORT  = 9111


def submit_task(command, queue):
  task = work_queue.Task(command)
  for source in ('shortest_path.py', 'graph.json', 'calculate_centrality.py'):
    task.specify_file(source, source, work_queue.WORK_QUEUE_INPUT)

  queue.submit(task)

def get_central_node(number_of_nodes):
  print "Starting ..."
  start = time.time()
  queue = work_queue.WorkQueue(PORT, name='graph-analysis-cunneen')

  scores_found = 0
  centrality_scores = {}
  with open("journal.json", "w") as f:
      json.dump(centrality_scores, f)
  scores_found = len(centrality_scores)

  for i in xrange(number_of_nodes):
    command = "./calculate_centrality.py -n %s -node %s" % (number_of_nodes, i)
    print "submitting command ...", command
    submit_task(command, queue)

  print "Submit workers to work_queue on port %d" %(PORT)
  while not queue.empty() and scores_found < number_of_nodes:
    task = queue.wait()
    if task and task.return_status==0:
      command = task.command.split(" ")
      node = command[-1]
      output = float(task.output.rstrip())
      centrality_scores[int(node)] = output
      print "command:", task.command, "\n\tcloseness centrality =", output
      with open("journal.json.new", "w") as j:
        json.dump(centrality_scores, j)
      os.rename('journal.json.new', 'journal.json')
    scores_found = len(centrality_scores)
  closest_node = 0
  closeness_score = centrality_scores[0]
  for i in xrange(1,number_of_nodes):
    if centrality_scores[i] > closeness_score:
      closest_node = i
      closeness_score = centrality_scores[i]

  print "The center of the graph is at node %d" %(closest_node)
  print "calculated in %d seconds" % (time.time() - start)


if __name__ == "__main__":
  num_nodes = int(sys.argv[1])
  get_central_node(num_nodes)
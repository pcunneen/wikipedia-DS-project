#!/usr/bin/python

import shortest_path
import json
import sys
import time

def centrality(graph, node_id, num_nodes):
  closeness_score = 0
  for i in xrange(num_nodes):
    to_node = i
    if i == node_id:
      continue
    distance = shortest_path.djikstra(graph, node_id, to_node)[-1]
    if distance > 0:
      closeness_score  += 1.0 / distance
  return closeness_score


if __name__ == "__main__":
  graph = {}
  with open("graph.json") as f:
    graph = json.load(f)

  new_graph = {}
  for key in graph:
    new_graph[int(key)] = (graph[key][0], graph[key][1])
  num_nodes = int(sys.argv[2])
  node = int(sys.argv[4])
  start = time.time()
  print centrality(new_graph, node, num_nodes)